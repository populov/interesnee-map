package com.haunted.interesnee;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;

import org.jetbrains.annotations.NotNull;

public class CustomAnimationActivity extends ActionBarActivity {
    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.fade_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_out, android.R.anim.slide_out_right);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean navigateUpTo(@NotNull Intent upIntent) {
        overridePendingTransition(android.R.anim.fade_out, android.R.anim.slide_out_right);
        return super.navigateUpTo(upIntent);
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        getSupportActionBar().setTitle(title);
    }
}
