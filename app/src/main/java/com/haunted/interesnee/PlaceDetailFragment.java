package com.haunted.interesnee;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.haunted.interesnee.model.ImageUrlCollection;
import com.haunted.interesnee.model.Place;
import com.haunted.interesnee.model.ThumbSize;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class PlaceDetailFragment extends PresenterFragment implements Presenter.IDetailsView,
        View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final int SELECT_IMAGE = 100;
    private static final int CAPTURE_IMAGE = 101;
    private static final String CAPTURE_IMAGE_FILE = "capturedImageFile";

    private EditText etName;
    private EditText etLatitude;
    private EditText etLongitude;
    private EditText etDate;

    private MyLocation myLocation;
    private Place placeToDisplay;
    private String capturedImageFile;
    private boolean restoreState;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_place_detail, container, false);
        assert v != null;

        etName = (EditText) v.findViewById(R.id.etName);
        etLatitude = (EditText) v.findViewById(R.id.etLatitude);
        etLongitude = (EditText) v.findViewById(R.id.etLongitude);
        etDate = (EditText) v.findViewById(R.id.etDate);

        v.findViewById(R.id.btnDate).setOnClickListener(this);
        v.findViewById(R.id.btnAddImage).setOnClickListener(this);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return v;
    }

    @Override
    protected void onPresenterCreated() {
        presenter.setDetailsView(this);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            capturedImageFile = savedInstanceState.getString(CAPTURE_IMAGE_FILE);
            restoreState = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter.isNewPlace())
            ensureMyLocationListener();
        if (restoreState)
            restoreState = false;
        else if (placeToDisplay != null)
            displayPlace(placeToDisplay);
        else if (!presenter.isNewPlace())
            displayPlace(presenter.getSelectedPlace());
        setHasOptionsMenu(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (myLocation != null)
            myLocation.stopListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.setDetailsView(Presenter.dummyDetails);
        presenter = null;
        myLocation = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CAPTURE_IMAGE_FILE, capturedImageFile);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, monthOfYear, dayOfMonth);
        TextView dateView = (TextView) getView().findViewById(R.id.etDate);
        Date date = c.getTime();
        dateView.setText(presenter.dateFormat.format(date));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.switch_to_map, menu);
        inflater.inflate(R.menu.place_createing, menu);
        inflater.inflate(R.menu.place_delete, menu);
        inflater.inflate(R.menu.image_capture, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem location = menu.findItem(R.id.menu_my_location);
        if (location != null)
            location.setVisible(presenter.isNewPlace());
        MenuItem save = menu.findItem(R.id.menu_save);
        if (save != null)
            save.setVisible(presenter.isNewPlace() && presenter.isTwoPane());
        MenuItem delete = menu.findItem(R.id.menu_delete);
        if (delete != null)
            delete.setVisible(!presenter.isNewPlace());
        MenuItem capture = menu.findItem(R.id.menu_camera);
        if (capture != null)
            capture.setVisible(!presenter.isNewPlace());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                presenter.deletePlaceIfConfirm(presenter.getSelectedPlaceId());
                return true;
            case R.id.menu_my_location:
                displayMyLocation();
                return true;
            case R.id.menu_save:
                presenter.savePlaceAndRun(new Runnable() {
                    @Override
                    public void run() {
                        displayPlace(presenter.getSelectedPlace());
                    }
                }, false);
                return true;
            case R.id.menu_map_view:
                presenter.savePlaceAndRun(new Runnable() {
                    @Override
                    public void run() {
                        presenter.showPlaceMap(presenter.getSelectedPlaceId());
                    }
                }, presenter.isTwoPane());
                return true;
            case R.id.menu_camera:
                presenter.savePlaceAndRun(new Runnable() {
                    @Override
                    public void run() {
                        captureImage();
                    }
                }, false);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDate:
                Calendar c = Calendar.getInstance();
                if (presenter.getSelectedPlaceId() != Place.NO_PLACE_ID)
                    c.setTime(presenter.getSelectedPlace().visited);
                DatePickerDialog dialog = new DatePickerDialog(getActivity(), this,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dialog.setCancelable(true);
                dialog.show();
                break;
            case R.id.btnAddImage:
                if (presenter.isNewPlace())
                    presenter.savePlaceAndRun(new Runnable() {
                        @Override
                        public void run() {
                            selectImage();
                        }
                    }, false);
                else
                    selectImage();
                break;
        }
    }

    @Override
    public void displayPlace(@NotNull Place place) {
        if (presenter == null) {
            placeToDisplay = place;
            return;
        }
        etName.setText(place.name);
        etDate.setText(presenter.dateFormat.format(place.visited));
        etLatitude.setText(place.id != Place.NO_PLACE_ID ? EnteredPlaceData.coordinateToString(place.latitude) : null);
        etLongitude.setText(place.id != Place.NO_PLACE_ID ? EnteredPlaceData.coordinateToString(place.longitude) : null);
        if (place.image != null && !place.image.isEmpty())
            renderImageThumbs(place.image);
        else if (place.id != Place.NO_PLACE_ID)
            renderMapThumb(place.latitude, place.longitude);
        else
            removeThumbs();
        placeToDisplay = null;
    }

    private void renderImageThumbs(String imageUrlsStr) {
        FragmentManager fm = getChildFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.lThumbs);
        ImageThumbsFragment thumbsImages = fragment != null && fragment instanceof ImageThumbsFragment ? (ImageThumbsFragment) fragment : null;
        if ((fragment != null && thumbsImages == null) || (thumbsImages != null && !imageUrlsStr.equals(thumbsImages.getDisplayedImageUrls()))) {
            fm.beginTransaction()
                    .remove(fragment)
                    .commitAllowingStateLoss();
            thumbsImages = null;
        }
        if (thumbsImages == null) {
            thumbsImages = ImageThumbsFragment.createInstance(imageUrlsStr);
            fm.beginTransaction()
                    .replace(R.id.lThumbs, thumbsImages)
                    .commitAllowingStateLoss();
        }
        else
            thumbsImages.displayThumbs(imageUrlsStr);
    }

    private void renderMapThumb(double latitude, double longitude) {
        FragmentManager fm = getChildFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.lThumbs);
        MapThumbFragment thumbFragment = fragment != null && fragment instanceof MapThumbFragment ? (MapThumbFragment) fragment : null;
        if ((fragment != null && thumbFragment == null) || (thumbFragment != null && thumbFragment.positionDifferent(latitude, longitude))) {
            fm.beginTransaction()
                    .remove(fragment)
                    .commit();
            thumbFragment = null;
        }
        if (thumbFragment == null) {
            thumbFragment = MapThumbFragment.createInstance(getActivity(), latitude, longitude, ThumbSize.LARGE);
            if (thumbFragment != null)
                fm.beginTransaction()
                        .replace(R.id.lThumbs, thumbFragment)
                        .commit();
        }
    }

    private void removeThumbs() {
        FragmentManager fm = getChildFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.lThumbs);
        if (fragment != null)
            fm.beginTransaction()
                    .remove(fragment)
                    .commit();
    }


    private void displayMyLocation() {
        ensureMyLocationListener();
        Location l = myLocation.get();
        if (l != null) {
            etLatitude.setText(EnteredPlaceData.coordinateToString(l.getLatitude()));
            etLongitude.setText(EnteredPlaceData.coordinateToString(l.getLongitude()));
            if (etName.getText() == null || etName.getText().toString() == null || etName.getText().toString().isEmpty())
                etName.setText(MapFragment.getDescription(getActivity(), new LatLng(l.getLatitude(), l.getLongitude())));
        }
        else
            Toast.makeText(getActivity(), R.string.cant_get_current_location, Toast.LENGTH_SHORT).show();
    }

    public EnteredPlaceData getPlaceData() {
        Place place = presenter.getSelectedPlace();
        return new EnteredPlaceData(etName, etLatitude, etLongitude, etDate, presenter.dateFormat, place);
    }

    private void selectImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_IMAGE);
    }

    private void saveSelectedImage(Intent imageIntent) {
        Uri selectedImage = imageIntent.getData();
        if (selectedImage != null)
            saveAndDisplayImage(selectedImage.toString());
    }

    private void captureImage() {
        try {
            File imageFile = File.createTempFile("interesnee-", ".jpg", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM));
            capturedImageFile = imageFile.getAbsolutePath();
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile.getAbsoluteFile()));
            startActivityForResult(intent, CAPTURE_IMAGE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveCapturedImage(Intent imageIntent) {
        if (imageIntent == null) {
            saveAndDisplayImage(capturedImageFile);
            return;
        }
        try {
            Uri imageUrl = imageIntent.getData();
            if (imageUrl != null)
                saveSelectedImage(imageIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveAndDisplayImage(String imageUri) {
        if (imageUri == null)
            return;
        ImageUrlCollection imageUrls = Place.addImage(presenter.getSelectedPlaceId(), imageUri, presenter.placesStorage, presenter.imageStorage);
        if (imageUrls.count() == 1)
            presenter.onPlacesUpdated();
        renderImageThumbs(imageUrls.toString());
    }

    private void ensureMyLocationListener() {
        if (myLocation == null)
            myLocation = new MyLocation(getActivity());
        myLocation.startListening();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageIntent) {
        ensurePresenter();
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case SELECT_IMAGE:
                    saveSelectedImage(imageIntent);
                    break;
                case CAPTURE_IMAGE:
                    saveCapturedImage(imageIntent);
                    break;
            }
        }
        else
            restoreState = true;
    }
}
