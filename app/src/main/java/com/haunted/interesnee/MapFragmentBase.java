package com.haunted.interesnee;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import org.jetbrains.annotations.NotNull;

public abstract class MapFragmentBase extends PresenterFragment {
    private SupportMapFragment mFragment;
    private boolean setupComplete;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FrameLayout v = new FrameLayout(getActivity());
        v.setId(R.id.mapView);
        createMapFragment();
        setupIfNeed(v);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupIfNeed(getView());
    }

    @Override
    public void onPause() {
        super.onPause();
        setupComplete = false;
    }

    private void createMapFragment() {
        FragmentManager fm = getChildFragmentManager();
        mFragment = SupportMapFragment.newInstance();
        fm.beginTransaction()
                .replace(R.id.mapView, mFragment, "map")
                .commit();
    }

    protected void setupIfNeed(final View rootView) {
        if (setupComplete || mFragment == null)
            return;
        GoogleMap map = mFragment.getMap();
        if (map != null) {
            setupMap(map);
            setupComplete = true;
        }
        else
            rootView.post(new Runnable() {
                @Override
                public void run() {
                    setupIfNeed(rootView);
                    setupComplete = true;
                }
            });
    }

    protected abstract void setupMap(@NotNull GoogleMap map);
}
