package com.haunted.interesnee;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.haunted.interesnee.model.Place;

public class PlaceListControllerTwoPane implements PlaceListActivity.PlaceListController {
    private static final String MAP = "map";
    private static final String DETAILS = "details";
    private static final String VIEW_MODE = "com.haunted.interesnee.viewMode";
    private final FragmentActivity activity;
    private Presenter presenter;
    private Presenter.IMapView mapFragment;
    private Presenter.IDetailsView detailFragment;
    private boolean forceShowDetails;

    public PlaceListControllerTwoPane(FragmentActivity activity, PlaceListFragment placeListFragment) {
        this.activity = activity;
        placeListFragment.setActivateOnItemClick(true);
    }

    @Override
    public void start(Presenter presenter, Bundle savedState) {
        this.presenter = presenter;
        String mode = null;
        if (savedState != null) {
            mode = savedState.getString(VIEW_MODE);
            presenter.setPlaceSelected(savedState.getInt(UiHelper.PLACE_ID));
        }
        if (mode == null)
            mode = presenter.getSelectedPlaceId() == Place.NO_PLACE_ID ? MAP : DETAILS;
        if (MAP.equals(mode))
            setMapFragment();
        else if (savedState != null)
            setDetailFragment();
        else
            showDetails(presenter.getSelectedPlaceId());
    }

    @Override
    public void showMap(int placeId) {
        if (mapFragment == null)
            setMapFragment();
        if (!presenter.isNewPlace())
            mapFragment.showPlace(placeId);
    }

    @Override
    public boolean showDetails(final int placeId) {
        if (detailFragment == null) {
            setDetailFragment();
            forceShowDetails = true;
        }
        if (forceShowDetails) {
            Place place = placeId != Place.NO_PLACE_ID ? presenter.placesStorage.getPlace(placeId) : Place.empty();
            detailFragment.displayPlace(place);
            forceShowDetails = false;
            return true;
        }
        final boolean[] canceled = {true};
        presenter.savePlaceAndRun(new Runnable() {
            @Override
            public void run() {
                canceled[0] = false;
                forceShowDetails = true;
                showDetails(placeId);
            }
        }, true);
        return !canceled[0];
    }

    @Override
    public void showList() {}

    @Override
    public void onPlaceDeleted(int deletedPlaceId) {
        if (!isMapMode() && presenter.getSelectedPlaceId() == deletedPlaceId)
            showMap(Place.NO_PLACE_ID);
    }

    @Override
    public boolean isMapMode() {
        return mapFragment != null;
    }

    @Override
    public boolean isTwoPane() {
        return true;
    }

    @Override
    public void onPlaceSelected() {
        activity.supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(MenuInflater menuInflater, Menu menu) {
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(UiHelper.PLACE_ID, presenter.getSelectedPlaceId());
        outState.putString(VIEW_MODE, isMapMode() ? MAP : DETAILS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    private void setMapFragment() {
        FragmentManager fm = activity.getSupportFragmentManager();
        mapFragment = (Presenter.IMapView) fm.findFragmentByTag(MAP);
        if (mapFragment == null)
            mapFragment = MapFragment.createInstance(activity);
        FragmentTransaction ft = fm.beginTransaction();
        if (detailFragment != null) {
            ft.remove((Fragment)detailFragment);
//                    .addToBackStack(MAP);
            detailFragment = null;
        }
        ft.replace(R.id.place_detail_container, (Fragment)mapFragment, MAP)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    private void setDetailFragment() {
        FragmentManager fm = activity.getSupportFragmentManager();
        detailFragment = (Presenter.IDetailsView) fm.findFragmentByTag(DETAILS);
        if (detailFragment == null)
            detailFragment = new PlaceDetailFragment();
        FragmentTransaction ft = fm.beginTransaction();
        if (mapFragment != null) {
            ft.remove((Fragment)mapFragment);
//                    .addToBackStack(DETAILS);
            mapFragment = null;
        }
        ft.replace(R.id.place_detail_container, (Fragment)detailFragment, DETAILS)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                .commit();
    }
}
