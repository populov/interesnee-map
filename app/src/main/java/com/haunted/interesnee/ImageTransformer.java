package com.haunted.interesnee;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.haunted.interesnee.model.ThumbSize;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ImageTransformer {
    private static Map<ThumbSize,Integer> sizes = new HashMap<>(3);

    public static Bitmap createThumb(Bitmap srcBmp, ThumbSize size, Context context) {
        int side = getActualThumbSize(size, context);
        Bitmap dstBmp;
        if (srcBmp.getWidth() >= srcBmp.getHeight()) {
            dstBmp = Bitmap.createBitmap(srcBmp,
                    srcBmp.getWidth()/2 - srcBmp.getHeight()/2,
                    0,
                    srcBmp.getHeight(),
                    srcBmp.getHeight()
            );
        } else {
            dstBmp = Bitmap.createBitmap(srcBmp,
                    0,
                    srcBmp.getHeight()/2 - srcBmp.getWidth()/2,
                    srcBmp.getWidth(),
                    srcBmp.getWidth()
            );
        }
        return Bitmap.createScaledBitmap(dstBmp, side, side, false);
    }

    public static int getActualThumbSize(ThumbSize size, Context context) {
        if (sizes.containsKey(size))
            return sizes.get(size);
        int result = (int) context.getResources().getDimension(size.resourceId);
        sizes.put(size, result);
        return result;
    }

    public static Bitmap readOrigin(Context context, InputStreamProvider input) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream in = input.getStream();
        if (in == null)
            return null;
        BitmapFactory.decodeStream(in, null, options);
        in.close();
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateInSampleSize(context, options);
        in = input.getStream();
        Bitmap result = null;
        if (in != null) {
            result = BitmapFactory.decodeStream(in, null, options);
            in.close();
        }
        return result;
    }

    public static int calculateInSampleSize(Context context, BitmapFactory.Options options) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);

        if (height > metrics.heightPixels || width > metrics.widthPixels) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > metrics.heightPixels
                    && (halfWidth / inSampleSize) > metrics.widthPixels) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public interface InputStreamProvider {
        InputStream getStream() throws IOException;
    }
}
