package com.haunted.interesnee.model;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.haunted.interesnee.data.ImageFileStorage;

public interface ImageStorage {
    Bitmap getExistingThumb(String path, ThumbSize size);
    Bitmap getExistingThumb(LatLng position, ThumbSize size);
    Bitmap getOrCreateThumb(String path, ThumbSize size, boolean allowDownload) throws ImageFileStorage.OriginNotFoundException;
    Bitmap getOrigin(String path, boolean allowDownload) throws ImageFileStorage.OriginNotFoundException;
    void deleteThumbs(String path);
    void deleteThumb(String first, ThumbSize size);
    void deleteThumbs(LatLng latLng);
    void saveMapThumb(LatLng position, ThumbSize size, Bitmap thumb);
}
