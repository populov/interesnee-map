package com.haunted.interesnee.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;
import java.util.Date;

public class Place {
    public static final int NO_PLACE_ID = -1;
    public final int id;
    public final String name;
    public final Date visited;
    public final double latitude;
    public final double longitude;
    public final String image;

    public Place(int id, String name, double latitude, double longitude, String image, Date visited) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.image = image;
        this.visited = visited;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Place))
            return false;

        Place p = (Place) o;

        boolean result = name == null ? (p.name == null) : name.equals(p.name);
        if (!result)
            return false;
        Calendar c = Calendar.getInstance();
        c.setTime(visited);

        Calendar pc = Calendar.getInstance();
        pc.setTime(p.visited);

        result = c.get(Calendar.YEAR) == pc.get(Calendar.YEAR)
                && c.get(Calendar.MONTH) == pc.get(Calendar.MONTH)
                && c.get(Calendar.DAY_OF_MONTH) == pc.get(Calendar.DAY_OF_MONTH);

        return result;
    }

    public static ImageUrlCollection addImage(int placeId, String imageUrl, IPlacesStorage placesStorage, ImageStorage imageStorage) {
        if (placeId == Place.NO_PLACE_ID)
            throw new IllegalArgumentException("Save place first");
        Place place = placesStorage.getPlace(placeId);
        ImageUrlCollection urls = new ImageUrlCollection(place.image);
        if (urls.count() == 0)
            imageStorage.deleteThumbs(new LatLng(place.latitude, place.longitude));
        urls.add(imageUrl);
        placesStorage.updatePlace(place.id, urls.toString());
        return urls;
    }

    public static void deleteImage(int placeId, String imageUrl, IPlacesStorage placesStorage, ImageStorage imageStorage) {
        Place place = placesStorage.getPlace(placeId);
        if (place != null) {
            ImageUrlCollection urls = new ImageUrlCollection(place.image);
            urls.remove(imageUrl);
            placesStorage.updatePlace(placeId, urls.toString());
        }
        imageStorage.deleteThumbs(imageUrl);
    }

    public static void setPrimaryImage(int placeId, String imageUrl, IPlacesStorage placesStorage, ImageStorage imageStorage) {
        Place place = placesStorage.getPlace(placeId);
        ImageUrlCollection urls = new ImageUrlCollection(place.image);
        imageStorage.deleteThumb(urls.first(), ThumbSize.MEDIUM);
        urls.setPrimary(imageUrl);
        placesStorage.updatePlace(placeId, urls.toString());
    }

    public static void updatePosition(int placeId, LatLng newPosition, IPlacesStorage placesStorage, ImageStorage imageStorage) {
        Place place = placesStorage.getPlace(placeId);
        LatLng oldPosition = new LatLng(place.latitude, place.longitude);
        imageStorage.deleteThumbs(oldPosition);
        placesStorage.updatePlace(placeId, newPosition.latitude, newPosition.longitude);
    }

    public static void deletePlace(int placeId, IPlacesStorage placesStorage, ImageStorage imageStorage) {
        Place place = placesStorage.getPlace(placeId);
        placesStorage.deletePlace(place.id);
        imageStorage.deleteThumbs(place.image);
    }

    public static Place empty() {
        return new Place(Place.NO_PLACE_ID, null, 0, 0, null, new Date());
    }
}
