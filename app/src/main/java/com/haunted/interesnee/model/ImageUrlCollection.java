package com.haunted.interesnee.model;

import java.util.ArrayList;
import java.util.List;

public class ImageUrlCollection {
    private static final String separator = "\n";
    private List<String> urls = new ArrayList<>();

    public ImageUrlCollection(String urlsJoined) {
        if (urlsJoined != null) {
            for (String s: urlsJoined.split(separator))
                if (!s.isEmpty())
                    urls.add(s);
        }
    }

    public String[] getAll() {
        String[] result = new String[urls.size()];
        return urls.toArray(result);
    }

    public void add(String url) {
        if (!urls.contains(url))
            urls.add(url);
    }

    public void remove(String url) {
        if (url != null && !url.isEmpty() && urls.contains(url))
            urls.remove(url);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String s: urls) {
            if (sb.length() > 0)
                sb.append(separator);
            sb.append(s);
        }
        return sb.toString();
    }

    public int count() {
        return urls.size();
    }

    public String first() {
        return urls.size() > 0 ? urls.get(0) : null;
    }

    public void setPrimary(String imageUrl) {
        urls.remove(imageUrl);
        ArrayList<String > result = new ArrayList<>(urls.size() + 1);
        result.add(imageUrl);
        result.addAll(urls);
        urls = result;
    }
}
