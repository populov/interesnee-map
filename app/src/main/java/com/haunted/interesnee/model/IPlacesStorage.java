package com.haunted.interesnee.model;

import java.util.Date;

public interface IPlacesStorage {
    Place[] getAllPlaces();
    Place getPlace(int id);

    int insertPlace(String name, double latitude, double longitude, String image, Date visited);
    void deletePlace(int id);
    boolean updatePlace(int id, String name, double latitude, double longitude, Date visited);
    boolean updatePlace(int id, double latitude, double longitude);
    boolean updatePlace(int id, String imageUrl);
}
