package com.haunted.interesnee.model;

import com.haunted.interesnee.R;

public enum ThumbSize {
    SMALL("thumbSmall", R.dimen.thumb_size_small),
    MEDIUM("thumbMedium", R.dimen.thumb_size_medium),
    LARGE("thumbLarge", R.dimen.thumb_size_large),
    FULL("full", 0);

    public static ThumbSize[] all = { SMALL, MEDIUM, LARGE, FULL };

    private ThumbSize(final String text, int resourceId) {
        this.text = text;
        this.resourceId = resourceId;
    }

    private final String text;
    public final int resourceId;

    @Override
    public String toString() {
        return text;
    }

    public static ThumbSize parse(String string) {
        for (ThumbSize size: all)
            if (size.toString().equals(string))
                return size;
        return null;
    }
}
