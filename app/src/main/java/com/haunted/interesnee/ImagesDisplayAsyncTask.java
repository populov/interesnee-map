package com.haunted.interesnee;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.haunted.interesnee.data.ImageFileStorage;
import com.haunted.interesnee.model.ImageStorage;
import com.haunted.interesnee.model.ThumbSize;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ImagesDisplayAsyncTask extends AsyncTask<String,Pair<Integer,Bitmap>,Void> {
    private final Context context;
    private final ImageStorage imageStorage;
    private final ViewGroup container;
    private final View.OnClickListener imageClickListener;
    private final ImagesNotFoundListener notFoundListener;
    private final int spacing;
    private List<String> notFoundImages;

    public ImagesDisplayAsyncTask(Context context, ImageStorage imageStorage, ViewGroup container, View.OnClickListener imageClickListener, ImagesNotFoundListener notFoundListener) {
        this.context = context;
        this.imageStorage = imageStorage;
        this.container = container;
        this.imageClickListener = imageClickListener;
        this.notFoundListener = notFoundListener;
        spacing = (int)context.getResources().getDimension(R.dimen.inputs_spacing);
    }

    @Override
    protected Void doInBackground(String... urls) {
        int i = 0;
        for (String imageUrl: urls) {
            try {
                Bitmap thumb = imageStorage.getOrCreateThumb(imageUrl, ThumbSize.LARGE, true);
                if (thumb != null)
                    publishProgress(new Pair<>(i, thumb));
            }
            catch (ImageFileStorage.OriginNotFoundException e) {
                if (notFoundImages == null)
                    notFoundImages = new ArrayList<>();
                notFoundImages.add(imageUrl);
                Log.e("interesnee.ImagesDisplayAsyncTask", "Original image not found " + imageUrl, e);
            }
            catch (Exception e) {
                Log.e("interesnee.ImagesDisplayAsyncTask", "Can't display thumbnail of " + imageUrl, e);
            }
            i++;
        }
        return null;
    }

    @SafeVarargs
    @Override
    protected final void onProgressUpdate(Pair<Integer, Bitmap>... thumbs) {
        ImageView iThumb = new ImageView(context);
        iThumb.setTag(thumbs[0].first);
        iThumb.setImageBitmap(thumbs[0].second);
        iThumb.setOnClickListener(imageClickListener);
        iThumb.setPadding(0, 0, spacing, 0);
        container.addView(iThumb);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (notFoundListener != null && notFoundImages != null)
            notFoundListener.onImagesNotFound(notFoundImages);
    }

    public interface ImagesNotFoundListener {
        void onImagesNotFound(Collection<String> notFoundImages);
    }
}