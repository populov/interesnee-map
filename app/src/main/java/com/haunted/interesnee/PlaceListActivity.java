package com.haunted.interesnee;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.crittercism.app.Crittercism;

public class PlaceListActivity extends CustomAnimationActivity implements PresenterFragment.IPresenterActivity {

    private PlaceListController controller;
    private Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crittercism.initialize(getApplicationContext(), "530f9f32647e752a26000006");
        setContentView(R.layout.activity_place_list);
        setTitle(R.string.my_places);

        int placeId = UiHelper.getPlaceId(getIntent());
        if (savedInstanceState != null)
            placeId = savedInstanceState.getInt(UiHelper.PLACE_ID, placeId);

        PlaceListFragment placeListFragment = (PlaceListFragment) getSupportFragmentManager().findFragmentById(R.id.place_list);
        controller = findViewById(R.id.place_detail_container) != null
                ? new PlaceListControllerTwoPane(this, placeListFragment)
                : new PlaceListControllerOnePane(this, placeListFragment);
        presenter = Presenter.createDefault(this, controller, placeId);
        controller.start(presenter, savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        controller.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return controller.onCreateOptionsMenu(getMenuInflater(), menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return controller.onOptionsItemSelected(item);
    }

    public Presenter getPresenter() { return presenter; }

    interface PlaceListController extends Presenter.IActivityView {
        void start(Presenter presenter, Bundle savedState);
        boolean onCreateOptionsMenu(MenuInflater menuInflater, Menu menu);
        void onSaveInstanceState(Bundle outState);
        boolean onOptionsItemSelected(MenuItem item);
    }
}
