package com.haunted.interesnee;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.haunted.interesnee.data.ImageFileStorage;
import com.haunted.interesnee.model.ImageStorage;
import com.haunted.interesnee.model.ThumbSize;

public class ImageLoadAsyncTask extends AsyncTask<Void,Void,Bitmap> {
    public final ImageView imageView;
    public final String url;
    private final ImageStorage storage;
    private final ThumbSize size;
    private final ImageNotFoundListener notFoundListener;
    private boolean notFound;

    public ImageLoadAsyncTask(ImageView imageView, String url, ImageStorage storage, ThumbSize size, ImageNotFoundListener notFoundListener){
        this.imageView = imageView;
        this.url = url;
        this.storage = storage;
        this.size = size;
        this.notFoundListener = notFoundListener;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            return storage.getOrCreateThumb(url, size, true);
        }
        catch (ImageFileStorage.OriginNotFoundException e) {
            notFound = true;
            Log.e("interesnee.ImageLoadAsyncTask", "Failed to load image", e);
            return null;
        }
        catch (Exception e) {
            Log.e("interesnee.ImageLoadAsyncTask", "Failed to load image", e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap thumb) {
        if (thumb != null)
            setImageThumb(imageView, thumb);
        if (notFound && notFoundListener != null)
            notFoundListener.onImageNotFound(url);
    }

    private void setImageThumb(ImageView image, Bitmap thumb) {
        if (image.getVisibility() != View.VISIBLE)
            image.setVisibility(View.VISIBLE);
        image.setImageBitmap(thumb);
    }

    interface ImageNotFoundListener {
        void onImageNotFound(String imageUrl);
    }
}
