package com.haunted.interesnee.data;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;

import com.haunted.interesnee.model.IPlacesStorage;
import com.haunted.interesnee.model.Place;

import java.util.ArrayList;
import java.util.List;

public class PlacesLoader extends AsyncTaskLoader<Place[]> {
    private static List<LoadComplete> completeCallbacks = new ArrayList<>();
    public static final int LOADER_ID = 101;
    private final IPlacesStorage storage;
    private Place[] mPlaces;

    public PlacesLoader(Context context, IPlacesStorage storage) {
        super(context);
        this.storage = storage;
    }

    @Override
    protected void onStartLoading() {
        if (mPlaces != null && !isReset())
            deliverResult(mPlaces);
        else
            forceLoad();
    }

    @Override
    public Place[] loadInBackground() {
        return mPlaces = storage.getAllPlaces();
    }

    public static void start(LoaderManager loaderManager, Callbacks callbacks) {
        if (completeCallbacks.size() > 1)
            loaderManager.initLoader(LOADER_ID, null, callbacks);
        else
            loaderManager.restartLoader(LOADER_ID, null, callbacks);
    }

    private static void handleCallbacks(Place[] places) {
        for(LoadComplete loadComplete: completeCallbacks)
            loadComplete.onPlacesLoaded(places);
        completeCallbacks.clear();
    }

    public static class Callbacks implements LoaderManager.LoaderCallbacks<Place[]> {
        private Context context;
        private IPlacesStorage placesStorage;

        public Callbacks(Context context, IPlacesStorage placesStorage, LoadComplete loadedCallback) {
            this.context = context;
            this.placesStorage = placesStorage;
            completeCallbacks.add(loadedCallback);
        }

        @Override
        public Loader<Place[]> onCreateLoader(int id, Bundle args) {
            return id == LOADER_ID ? new PlacesLoader(context, placesStorage) : null;
        }

        @Override
        public void onLoadFinished(Loader<Place[]> loader, Place[] places) {
            handleCallbacks(places);
        }

        @Override
        public void onLoaderReset(Loader<Place[]> loader) {}
    }

    public interface LoadComplete {
        void onPlacesLoaded(Place[] places);
    }
}
