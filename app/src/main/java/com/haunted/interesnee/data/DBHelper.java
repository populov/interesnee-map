package com.haunted.interesnee.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.haunted.interesnee.model.Place;

public class DBHelper extends SQLiteOpenHelper {
    private static final int currentVersion = 1;
    private static final String dbName = "placesDb";

    public DBHelper(Context context) {
        super(context, dbName, null, currentVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        PlacesDbStorage.createTable(db);
        Place[] sampleData = new SampleDownloader().getPlaces();
        PlacesDbStorage.insert(db, sampleData);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}
