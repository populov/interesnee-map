package com.haunted.interesnee.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.haunted.interesnee.ImageTransformer;
import com.haunted.interesnee.model.ImageStorage;
import com.haunted.interesnee.model.ThumbSize;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

public class ImageFileStorage implements ImageStorage {
    private final Context context;
    private static final String TAG = "interesnee";
    private static final String APP_NAME = "interesnee-map";
    private static final String IMAGES = "images";

    public ImageFileStorage(Context context) {
        this.context = context;
    }

    @Override
    public Bitmap getExistingThumb(String path, ThumbSize size) {
        File thumbFile = getThumbFileName(path, size);
        if (thumbFile.exists())
            return BitmapFactory.decodeFile(thumbFile.getPath());
        return null;
    }

    @Override
    public Bitmap getExistingThumb(LatLng position, ThumbSize size) {
        File mapThumbFile = getMapThumbFileName(position, size);
        if (mapThumbFile.exists())
            return BitmapFactory.decodeFile(mapThumbFile.getPath());
        return null;
    }

    @Override
    public Bitmap getOrCreateThumb(String path, ThumbSize size, boolean allowDownload) throws OriginNotFoundException {
        File thumbFile = getThumbFileName(path, size);
        if (thumbFile.exists())
            return BitmapFactory.decodeFile(thumbFile.getPath());
        Bitmap origin = getOrigin(path, allowDownload);
        if (origin == null)
            throw new OriginNotFoundException("Origin not found", null);
        if (size == ThumbSize.FULL)
            return origin;
        Bitmap thumb = ImageTransformer.createThumb(origin, size, context);
        if (thumb != null)
            saveBitmap(thumb, thumbFile);
        return thumb;
    }

    @Override
    public void deleteThumbs(String path) {
        if (path == null)
            return;
        for (ThumbSize size: ThumbSize.all)
            deleteThumb(path, size);
    }

    @Override
    public void deleteThumb(@NotNull String path, ThumbSize size) {
        File file = getThumbFileName(path, size);
        if (file.exists() && !file.delete())
            Log.w(TAG, "Couldn't delete file " + file);
    }

    @Override
    public void deleteThumbs(LatLng position) {
        for (ThumbSize size: ThumbSize.all) {
            File file = getMapThumbFileName(position, size);
            if (file.exists() && !file.delete())
                Log.w(TAG, "Couldn't delete file " + file);
        }
    }

    @Override
    public void saveMapThumb(LatLng position, ThumbSize size, Bitmap thumb) {
        File file = getMapThumbFileName(position, size);
        saveBitmap(thumb, file);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void saveBitmap(Bitmap image, File file){
        try {
            ensureParentDirectory(file);
            FileOutputStream outStream = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, 90, outStream);
            outStream.close();
        } catch (IOException e) {
            Log.e(TAG, "Couldn't write bitmap file: " + file, e);
        }
    }

    public Bitmap getOrigin(String path, boolean allowDownload) throws OriginNotFoundException {
        if (path == null)
            return null;
        if (path.toLowerCase(Locale.US).startsWith("http"))
            return getOriginRemote(path, allowDownload);
        else
            return getOriginLocal(path);
    }

    private Bitmap getOriginRemote(String path, boolean allowDownload) {
        File origin = getDownloadedFileName(path);
        if (origin.exists())
            return BitmapFactory.decodeFile(origin.getPath());
        if (!allowDownload)
            return null;
        byte[] content = download(path);
        if (content != null)
            return BitmapFactory.decodeByteArray(content, 0, content.length);
        return null;
    }

    private Bitmap getOriginLocal(@NotNull final String path) throws OriginNotFoundException {
        try {
            if (path.startsWith("content:"))
                return ImageTransformer.readOrigin(context, new ImageTransformer.InputStreamProvider() {
                    @Override
                    public InputStream getStream() throws IOException {
                        Uri uri = Uri.parse(path);
                        return context.getContentResolver().openInputStream(uri);
                    }
                });
            else return ImageTransformer.readOrigin(context, new ImageTransformer.InputStreamProvider() {
                @Override
                public InputStream getStream() throws IOException {
                    return new FileInputStream(path);
                }
            });
        }
        catch (IOException e) {
            OriginNotFoundException notFoundException = new OriginNotFoundException("Couldn't get local image: " + path, e);
            Log.e(TAG, notFoundException.getMessage(), e);
            throw notFoundException;
        }
    }

    private byte[] download(String path) {
        File tempFile;
        HttpURLConnection connection = null;
        try {
            tempFile = getTempDownloadFile();
            connection = (HttpURLConnection) new URL(path).openConnection();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                connection.disconnect();
                Log.e(TAG, "Couldn't download file: " + path);
                return null;
            }
        }
        catch (IOException e) {
            Log.e(TAG, "Couldn't download file: " + path, e);
            if (connection != null)
                connection.disconnect();
            return null;
        }
        try {
            int bufferSize = connection.getContentLength() > 2048 ? 2048 : connection.getContentLength();
            InputStream in = connection.getInputStream();
            FileOutputStream fileOut = new FileOutputStream(tempFile);
            ByteArrayOutputStream memOut = new ByteArrayOutputStream(connection.getContentLength());
            byte[] buffer = new byte[bufferSize];
            int bytesRead;
            while((bytesRead = in.read(buffer, 0, bufferSize)) >= 0){
                fileOut.write(buffer, 0, bytesRead);
                memOut.write(buffer, 0, bytesRead);
            }
            fileOut.flush();
            fileOut.close();
            File downloadedFile = getDownloadedFileName(path);
            ensureParentDirectory(downloadedFile);
            if (!tempFile.renameTo(downloadedFile))
                Log.e(TAG, "Couldn't rename file " + tempFile + " to " + downloadedFile);
            return memOut.toByteArray();
        } catch (Exception e) {
            Log.e(TAG, "Couldn't save file: " + path, e);
            return null;
        }
        finally {
            connection.disconnect();
            if (tempFile.exists() && !tempFile.delete())
                Log.e(TAG, "Couldn't delete file: " + tempFile);
        }
    }

    private File getTempDownloadFile() throws IOException {
        File tempDir = context.getCacheDir();
        return File.createTempFile("img", null, tempDir);
    }

    private File getDownloadedFileName(String url) {
        return new File(context.getCacheDir(), pathJoin(APP_NAME, IMAGES, "full", urlHash(url) + getExt(url)));
    }

    private File getThumbFileName(String url, ThumbSize size) {
        if (size == ThumbSize.FULL)
            return getDownloadedFileName(url);
        return new File(context.getCacheDir(), pathJoin(APP_NAME, IMAGES, size.toString(), urlHash(url) + ".png"));
    }

    private File getMapThumbFileName(LatLng position, ThumbSize size) {
        return new File(context.getCacheDir(), pathJoin(APP_NAME, IMAGES, size.toString(), Integer.toHexString(position.hashCode()) + ".png"));
    }

    private String getExt(String url) {
        int dotPos = url.lastIndexOf('.');
        if (dotPos < 0 || url.length() - dotPos > 5)
            return "";
        return url.substring(dotPos);
    }

    private String pathJoin(String... pathEntries){
        StringBuilder result = new StringBuilder();
        for (String entry: pathEntries){
            result.append(entry).append(File.separatorChar);
        }
        return result.toString();
    }

    private String urlHash(String url) {
        return Integer.toHexString(url.hashCode());
    }

    private void ensureParentDirectory(File filePath) {
        File parentDir = filePath.getParentFile();
        if (!parentDir.exists())
            if (!parentDir.mkdirs())
                Log.w(TAG, "Couldn't create directory" + parentDir);
    }

    public static class OriginNotFoundException extends FileNotFoundException {
        public OriginNotFoundException(String detailMessage, Exception e) {
            super(detailMessage);
            if (e != null)
                initCause(e);
        }
    }
}
