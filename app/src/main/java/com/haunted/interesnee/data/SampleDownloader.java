package com.haunted.interesnee.data;

import com.haunted.interesnee.model.Place;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SampleDownloader {
    private static final String sampleDataUrl = "http://interesnee.ru/files/android-middle-level-data.json";
    private DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);

    public Place[] getPlaces() {
        try {
            String response = getURLResponse(sampleDataUrl);
            return parseJsonResponse(response);
        } catch (IOException|JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Place[] parseJsonResponse(String response) throws JSONException {
        if (response == null)
            return null;
        JSONObject jsonObject = new JSONObject(response);
        JSONArray places = jsonObject.getJSONArray("places");
        Place[] result = new Place[places.length()];
        for (int i = 0; i < places.length(); i++) {
            JSONObject place = places.getJSONObject(i);
            String name = place.getString("text");
            double latitude = place.getDouble("latitude");
            double longitude = place.getDouble("longtitude");
            String image = place.getString("image");
            String lastVisited = place.getString("lastVisited");
            Date visited;
            try {
                visited = format.parse(lastVisited);
            } catch (ParseException e) {
                e.printStackTrace();
                visited = new Date();
            }
            result[i] = new Place(Place.NO_PLACE_ID, name, latitude, longitude, image, visited);
        }
        return result;
    }

    private String getURLResponse(String path) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(path).openConnection();
        conn.setDoOutput(false);

        String encoding = conn.getContentEncoding();
        encoding = encoding != null ? encoding : "UTF-8";
        String result = readStreamToString(conn.getInputStream(), encoding, conn.getContentLength());
        conn.disconnect();
        return result;
    }

    public static String readStreamToString(InputStream inputStream, String encoding, int bufferSize) {
        if (bufferSize <= 0)
            bufferSize = 65535;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        try {
            Reader in = new InputStreamReader(inputStream, encoding);
            int bytesRead;
            do {
                bytesRead = in.read(buffer, 0, buffer.length);
                if (bytesRead < 0)
                    break;
                out.append(buffer, 0, bytesRead);
            } while (bytesRead > 0);
        }
        catch (IOException ex) {
            return null;
        }
        return out.toString();
    }
}
