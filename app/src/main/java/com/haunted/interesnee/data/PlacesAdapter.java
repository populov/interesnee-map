package com.haunted.interesnee.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.haunted.interesnee.ImageLoadAsyncTask;
import com.haunted.interesnee.R;
import com.haunted.interesnee.model.ImageStorage;
import com.haunted.interesnee.model.ImageUrlCollection;
import com.haunted.interesnee.model.Place;
import com.haunted.interesnee.model.ThumbSize;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;

public class PlacesAdapter extends BaseAdapter {
    private final Place[] places;
    private LayoutInflater layoutInflater;
    private DateFormat dateFormat;
    private ImageStorage imageStorage;
    private ListView listView;

    public PlacesAdapter(ListView listView, Place[] places, ImageStorage imageStorage, DateFormat dateFormat) {
        this.listView = listView;
        this.imageStorage = imageStorage;
        if (places == null)
            throw new NullPointerException("Places array is null");
        this.places = places;
        this.dateFormat = dateFormat;
        Context context = listView.getContext();
        if (context != null)
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return places.length;
    }

    @Override
    public Object getItem(int position) {
        return getPlace(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView != null ? convertView : layoutInflater.inflate(R.layout.place_list_item, null);
        assert v != null;
        Place place = getPlace(position);
        ((TextView)v.findViewById(R.id.tName)).setText(place.name);
        ((TextView)v.findViewById(R.id.tDate)).setText(dateFormat.format(place.visited));
        ImageView image = (ImageView) v.findViewById(R.id.iThumb);
        if (convertView != null)
            image.setImageResource(R.drawable.ic_image_placeholder);
        setImageThumb(image, place, imageStorage);
        updateRawBackground(v, position);
        return v;
    }

    public void updateRawBackground(View row, int position) {
        boolean checked = listView.isItemChecked(position);
        row.setBackgroundResource(checked ? R.color.list_selected_background : R.color.list_background);
    }

    public static void setImageThumb(@NotNull ImageView image, Place place, ImageStorage imageStorage) {
        String url = new ImageUrlCollection(place.image).first();
        Bitmap thumb = url != null
                ? imageStorage.getExistingThumb(url, ThumbSize.MEDIUM)
                : imageStorage.getExistingThumb(new LatLng(place.latitude, place.longitude), ThumbSize.MEDIUM);
        if (thumb != null)
            image.setImageBitmap(thumb);
        else if (url != null)
            new ImageLoadAsyncTask(image, url, imageStorage, ThumbSize.MEDIUM, null).execute();
    }

    public Place getPlace(int position) {
        return places[position];
    }

    public int getPosition(int placeId) {
        for(int i = 0; i < places.length; i++)
            if (places[i].id == placeId)
                return i;
        return ListView.INVALID_POSITION;
    }
}
