package com.haunted.interesnee.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.haunted.interesnee.model.IPlacesStorage;
import com.haunted.interesnee.model.Place;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PlacesDbStorage implements IPlacesStorage {
    private static final String tableName = "Places";
    private static final String _id = "id";
    private static final String _name = "name";
    private static final String _latitude = "latitude";
    private static final String _longitude = "longitude";
    private static final String _image = "image";
    private static final String _visited = "visited";

    private static final String[] allColumns = new String[] { _id, _name, _latitude, _longitude, _image, _visited };

    private static int idIndex = indexOf(allColumns,  _id);
    private static int nameIndex = indexOf(allColumns,  _name);
    private static int latitudeIndex = indexOf(allColumns,  _latitude);
    private static int longitudeIndex = indexOf(allColumns,  _longitude);
    private static int imageIndex = indexOf(allColumns,  _image);
    private static int visitedIndex = indexOf(allColumns,  _visited);

    private DBHelper dbHelper;

    public PlacesDbStorage(Context context) {
        dbHelper = new DBHelper(context);
    }

    @Override
    public Place[] getAllPlaces() {
        return getPlaces(null, null);
    }

    @Override
    public Place getPlace(int id) {
        Place[] places = getPlaces(_id + "=" + id, null);
        if (places == null || places.length == 0)
            return null;
        return places[0];
    }

    private Place[] getPlaces(String whereClause, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (db == null)
            throw new NullPointerException("Can't get places storage");
        Place[] result;
        try {
            Cursor cursor = db.query(tableName, allColumns, whereClause, selectionArgs, null, null, null);
            result = cursorToHabitArray(cursor);
        } catch (Exception e) {
            e.printStackTrace();
            return new Place[0];
        }
        finally {
            db.close();
        }
        return result;
    }


    private Place[] cursorToHabitArray(Cursor c) {
        List<Place> habits = new ArrayList<>();
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            habits.add(readPlace(c));
        }
        c.close();
        Place[] result = new Place[habits.size()];
        habits.toArray(result);
        return result;
    }

    private Place readPlace(Cursor c) {
        int id = c.getInt(idIndex);
        String name = c.getString(nameIndex);
        double latitude = c.getDouble(latitudeIndex);
        double longitude = c.getDouble(longitudeIndex);
        String image = c.getString(imageIndex);
        Date visited = new Date(c.getLong(visitedIndex));
        return new Place(id, name, latitude, longitude, image, visited);
    }

    @Override
    public int insertPlace(String name, double latitude, double longitude, String image, Date visited) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (db == null)
            throw new NullPointerException("Can't get habits storage");
        try {
            return (int)insertPlace(db, name, latitude, longitude, image, visited);
        }
        finally {
            db.close();
        }
    }

    private static long insertPlace(SQLiteDatabase db, String name, double latitude, double longitude, String image, Date visited) {
        ContentValues cv = new ContentValues();
        cv.put(_name, name);
        cv.put(_latitude, latitude);
        cv.put(_longitude, longitude);
        cv.put(_image, image);
        cv.put(_visited, visited.getTime());
        return db.insert(tableName, null, cv);
    }

    @Override
    public void deletePlace(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (db == null)
            throw new NullPointerException("Can't get places storage");
        try {
            db.delete(tableName, _id + "=" + id, null);
        }
        finally {
            db.close();
        }
    }

    @Override
    public boolean updatePlace(int id, String name, double latitude, double longitude, Date visited) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (db == null)
            throw new NullPointerException("Can't get places storage");
        try {
            ContentValues cv = new ContentValues();
            cv.put(_name, name);
            cv.put(_latitude, latitude);
            cv.put(_longitude, longitude);
            cv.put(_visited, visited.getTime());
            return 0 < db.update(tableName, cv, _id + "=" + id, null);
        }
        finally {
            db.close();
        }
    }

    @Override
    public boolean updatePlace(int id, double latitude, double longitude) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (db == null)
            throw new NullPointerException("Can't get places storage");
        try {
            ContentValues cv = new ContentValues();
            cv.put(_latitude, latitude);
            cv.put(_longitude, longitude);
            return 0 < db.update(tableName, cv, _id + "=" + id, null);
        }
        finally {
            db.close();
        }
    }

    @Override
    public boolean updatePlace(int id, String imageUrl) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (db == null)
            throw new NullPointerException("Can't get places storage");
        try {
            ContentValues cv = new ContentValues();
            cv.put(_image, imageUrl);
            return 0 < db.update(tableName, cv, _id + "=" + id, null);
        }
        finally {
            db.close();
        }
    }
    private static int indexOf(String[] array, String value) {
        for (int i = 0; i < array.length; i++)
            if (array[i].equalsIgnoreCase(value))
                return i;
        return -1;
    }

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + tableName + " ("
                + _id + " integer PRIMARY KEY AUTOINCREMENT, "
                + _name + " text, "
                + _latitude + " real, "
                + _longitude + " real, "
                + _image + " text, "
                + _visited + " int"
                + ")");
    }

    public static void insert(SQLiteDatabase db, Place[] places) {
        if (places == null)
            return;
        for (Place place: places)
            insertPlace(db, place.name, place.latitude, place.longitude, place.image, place.visited);
    }
}
