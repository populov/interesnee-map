package com.haunted.interesnee;

import android.content.Context;
import android.content.Intent;

import com.haunted.interesnee.model.Place;

import org.jetbrains.annotations.NotNull;

public class UiHelper {
    public static final String PLACE_ID = "place_id";

    public static int getPlaceId(Intent intent) {
        return intent.getIntExtra(PLACE_ID, Place.NO_PLACE_ID);
    }

    public static Intent mapIntent(Context context, int placeId) {
        return getPlaceIntent(context, MapActivity.class, placeId);
    }

    public static Intent detailsIntent(Context context, int placeId) {
        return getPlaceIntent(context, PlaceDetailActivity.class, placeId);
    }

    public static Intent listIntent(Context context, int placeId) {
        return getPlaceIntent(context, PlaceListActivity.class, placeId);
    }

    public static Intent imageViewIntent(Context context, int placeId, int imageIndex) {
        Intent imageIntent = getPlaceIntent(context, ImageViewActivity.class, placeId);
        imageIntent.putExtra(ImageViewActivity.IMAGE_INDEX, imageIndex);
        return imageIntent;
    }

    private static Intent getPlaceIntent(@NotNull Context context, @NotNull Class<?> cls, int placeId) {
        Intent intent = new Intent(context, cls);
        intent.putExtra(PLACE_ID, placeId);
        return intent;
    }
}
