package com.haunted.interesnee;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.haunted.interesnee.model.ImageStorage;
import com.haunted.interesnee.model.ThumbSize;

import org.jetbrains.annotations.NotNull;

public class MapThumbFragment extends MapFragmentBase implements GoogleMap.SnapshotReadyCallback, GoogleMap.OnMapLoadedCallback, View.OnClickListener, GoogleMap.OnMapClickListener {
    private static final String POSITION = "position";
    private static final String SIZE = "size";
    private static final float MAP_ZOOM = 15;
    private ImageStorage imageStorage;
    private LatLng position;
    private GoogleMap map;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ensurePresenter();
        Bundle args = getArguments();
        ThumbSize size = ThumbSize.parse(args.getString(SIZE));
        position = args.getParcelable(POSITION);
        Bitmap thumb = imageStorage != null ? imageStorage.getExistingThumb(position, size) : null;
        if (thumb != null) {
            ImageView mapThumbImage = new ImageView(getActivity());
            mapThumbImage.setImageBitmap(thumb);
            mapThumbImage.setOnClickListener(this);
            return mapThumbImage;
        }

        FrameLayout v = (FrameLayout) super.onCreateView(inflater, container, savedInstanceState);
        setLayoutParams(v, size);
        return v;
    }

    private void setLayoutParams(FrameLayout rootView, ThumbSize size) {
        int actualSize = ImageTransformer.getActualThumbSize(size, getActivity());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(actualSize, actualSize);
        rootView.setLayoutParams(params);
    }

    @Override
    protected void onPresenterCreated() {
        imageStorage = presenter.imageStorage;
    }

    @Override
    public void onMapLoaded() {
        map.snapshot(this);
    }

    @Override
    public void onSnapshotReady(Bitmap bitmap) {
        if (bitmap != null) {
            imageStorage.saveMapThumb(position, ThumbSize.LARGE, bitmap);
            Bitmap listThumb = ImageTransformer.createThumb(bitmap, ThumbSize.MEDIUM, getActivity());
            imageStorage.saveMapThumb(position, ThumbSize.MEDIUM, listThumb);
            presenter.onPlacesUpdated();
        }
    }

    @Override
    public void onClick(View v) {
        openMapView();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        openMapView();
    }

    @Override
    protected void setupMap(@NotNull GoogleMap map) {
        this.map = map;
        if (position != null)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, MAP_ZOOM));
        UiSettings uiSettings = map.getUiSettings();
        uiSettings.setZoomControlsEnabled(false);
        uiSettings.setAllGesturesEnabled(false);
        map.setOnMapLoadedCallback(this);
        map.setOnMapClickListener(this);
    }

    private void openMapView() {
        presenter.showPlaceMap(presenter.getSelectedPlaceId());
    }

    public boolean positionDifferent(double latitude, double longitude) {
        return position.latitude != latitude || position.longitude != longitude;
    }

    public static MapThumbFragment createInstance(Context context, double latitude, double longitude, ThumbSize size) {
        if (ConnectionResult.SUCCESS == GooglePlayServicesUtil.isGooglePlayServicesAvailable(context)) {
            Bundle args = new Bundle();
            args.putString(SIZE, size.toString());
            args.putParcelable(POSITION, new LatLng(latitude, longitude));
            MapThumbFragment result = new MapThumbFragment();
            result.setArguments(args);
            return result;
        }
        else
            return null;
    }
}
