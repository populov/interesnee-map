package com.haunted.interesnee;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.haunted.interesnee.data.PlacesAdapter;
import com.haunted.interesnee.data.PlacesLoader;
import com.haunted.interesnee.model.Place;

public class PlaceListFragment extends ListFragment implements Presenter.IListView,
        AdapterView.OnItemLongClickListener, PlacesLoader.LoadComplete {

    private static final String STATE_VISIBLE_POSITION = "activated_position";
    private Presenter presenter;
    private ListView listView;
    private PlacesAdapter placesAdapter;
    private ActionMode actionMode;
    private int placeToSelect = Place.NO_PLACE_ID;
    private int scrollToPosition = ListView.INVALID_POSITION;
    private int selectedPosition = ListView.INVALID_POSITION;

    public PlaceListFragment() {}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setBackgroundResource(R.color.list_background);
        listView = getListView();
        listView.setOnItemLongClickListener(this);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null)
            scrollToPosition = savedInstanceState.getInt(STATE_VISIBLE_POSITION, ListView.INVALID_POSITION);
    }

    @Override
    public void onResume() {
        super.onResume();
        ensurePresenter();
        updatePlacesList();
        setHasOptionsMenu(true);
    }

    private void ensurePresenter() {
        if (presenter == null) {
            presenter = ((PlaceListActivity)getActivity()).getPresenter();
            presenter.setListView(this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null)
            outState.putInt(STATE_VISIBLE_POSITION, listView.getFirstVisiblePosition());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_new, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem addNewPlace = menu.findItem(R.id.menu_add);
        if (addNewPlace != null)
            addNewPlace.setVisible(!(presenter.isTwoPane() && presenter.isNewPlace()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {
            presenter.addNewPlace();
            return true;
        }
        return false;
    }

    @Override
    public void updatePlacesList() {
        if (scrollToPosition == ListView.INVALID_POSITION)
            scrollToPosition = listView.getFirstVisiblePosition();
        presenter.getPlaces(this);
    }

    @Override
    public void onPlacesLoaded(Place[] places) {
        placesAdapter = new PlacesAdapter(listView, places, presenter.imageStorage, presenter.dateFormat);
        setListAdapter(placesAdapter);
        if (placeToSelect != Place.NO_PLACE_ID) {
            scrollToPosition = getPositionByPlaceId(placeToSelect);
            setItemSelected(placeToSelect, true);
            placeToSelect = Place.NO_PLACE_ID;
        }
        else
            setItemSelected(presenter.getSelectedPlaceId(), true);
    }

    @Override
    public void setItemSelected(int placeId) {
        if (placesAdapter == null)
            placeToSelect = placeId;
        else {
            scrollToPosition = getPositionByPlaceId(placeId);
            setItemSelected(placeId, true);
        }
    }

    private void setItemSelected(int placeId, boolean scrollToItem) {
        int position = getPositionByPlaceId(placeId);
        if (position == ListView.INVALID_POSITION)
            listView.setItemChecked(selectedPosition, false);
        else
            listView.setItemChecked(position, true);
        if (scrollToItem)
            scrollToPosition(scrollToPosition);
        selectedPosition = position;
    }

    private void scrollToPosition(int position) {
        if (position == ListView.INVALID_POSITION)
            return;
        if (presenter.isTwoPane())
            listView.smoothScrollToPosition(position);
        else
            listView.setSelection(position);
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        if (actionMode == null)
            presenter.navigateToPlace(getPlaceIdByPosition(position), false);
        else
            placesAdapter.updateRawBackground(view, position);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (actionMode == null) {
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            ((ActionBarActivity) getActivity()).startSupportActionMode(actionModeCallback);
        }
        setItemSelected(placesAdapter.getPlace(position).id, false);
        return true;
    }

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        listView.setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private int getPlaceIdByPosition(int position) {
        return placesAdapter.getPlace(position).id;
    }

    private int getPositionByPlaceId(int placeId) {
        return placesAdapter.getPosition(placeId);
    }

    ActionMode.Callback actionModeCallback = new ActionMode.Callback()
    {
        int previousChoiceMode;
        @Override
        public boolean onCreateActionMode (ActionMode mode, Menu menu){
            actionMode = mode;
            mode.getMenuInflater().inflate(R.menu.place_delete, menu);
            mode.setTitle(R.string.select_places_to_delete);
            previousChoiceMode = listView.getChoiceMode();
            return true;
        }

        @Override
        public boolean onPrepareActionMode (ActionMode mode, Menu menu){
            return false;
        }

        @Override
        public boolean onActionItemClicked (ActionMode mode, MenuItem item){
            if (item.getItemId() == R.id.menu_delete) {
                PlacesAdapter placesAdapter = (PlacesAdapter) getListAdapter();
                int selectedPlaceId = presenter.getSelectedPlaceId();
                int deletedCount = 0;
                boolean deleteSelected = false;
                for (int i = 0; i < placesAdapter.getCount(); i++) {
                    if (listView.isItemChecked(i)) {
                        int placeId = placesAdapter.getPlace(i).id;
                        Place.deletePlace(placeId, presenter.placesStorage, presenter.imageStorage);
                        if (placeId == selectedPlaceId)
                            deleteSelected = true;
                        deletedCount++;
                    }
                }
                if (deleteSelected)
                    presenter.onPlaceDeleted(selectedPlaceId);
                if (deletedCount > 0)
                    presenter.onPlacesUpdated();
                mode.finish();
            }
            return false;
        }

        @Override
        public void onDestroyActionMode (ActionMode mode){
            listView.clearChoices();
            for (int i = 0; i < listView.getCount(); i++)
                listView.setItemChecked(i, false);
            listView.post(new Runnable() {
                @Override
                public void run() {
                    listView.setChoiceMode(previousChoiceMode);
                    setItemSelected(presenter.getSelectedPlaceId(), false);
                }
            });
            actionMode = null;
        }
    };
}
