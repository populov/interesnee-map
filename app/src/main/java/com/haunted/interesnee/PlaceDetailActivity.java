package com.haunted.interesnee;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.haunted.interesnee.model.Place;

public class PlaceDetailActivity extends PresenterActivity implements Presenter.IActivityView {
    boolean noSavedState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);
        noSavedState = savedInstanceState == null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        FragmentManager fm = getSupportFragmentManager();
        PlaceDetailFragment placeDetails = (PlaceDetailFragment) fm.findFragmentByTag("details");
        if (placeDetails == null) {
            placeDetails = new PlaceDetailFragment();
            fm.beginTransaction()
                    .add(R.id.place_detail_container, placeDetails, "details")
                    .commit();
        }
        Place place = presenter.getSelectedPlace();
        setTitle(place != null ? place.name : getString(R.string.new_place));
        if (noSavedState)
            placeDetails.displayPlace(place != null ? place : Place.empty());
    }

    @Override
    public void onBackPressed() {
        presenter.savePlaceAndRun(new Runnable() {
            @Override
            public void run() {
                PlaceDetailActivity.super.onBackPressed();
            }
        }, true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                presenter.savePlaceAndRun(new Runnable() {
                    @Override
                    public void run() {
                        showList();
                    }
                }, true);
                return true;
        }
        return false;
    }

    @Override
    public void showMap(int placeId) {
        startActivity(UiHelper.mapIntent(this, placeId));
    }

    @Override
    public boolean showDetails(int placeId) {
        throw new UnsupportedOperationException("showDetails() should never be called on PlaceDetailActivity");
    }

    @Override
    public void showList() {
        NavUtils.navigateUpTo(this, UiHelper.listIntent(this, presenter.getSelectedPlaceId()));
    }

    @Override
    public void onPlaceDeleted(int placeId) {
        showList();
    }

    @Override
    public void onPlaceSelected() {
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean isMapMode() {
        return false;
    }

    @Override
    public boolean isTwoPane() {
        return false;
    }
}
