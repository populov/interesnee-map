package com.haunted.interesnee;

import android.app.AlertDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.haunted.interesnee.data.PlacesLoader;
import com.haunted.interesnee.model.Place;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapFragment extends MapFragmentBase implements Presenter.IMapView,
        PlacesLoader.LoadComplete {
    public static final String CAMERA_POSITION = "cameraPosition";

    private CameraPosition cameraPosition;
    private MarkerManager markerManager;
    private int placeIdToShow = Place.NO_PLACE_ID;
    private GoogleMap map;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            cameraPosition = savedInstanceState.getParcelable(CAMERA_POSITION);
            placeIdToShow = savedInstanceState.getInt(UiHelper.PLACE_ID, Place.NO_PLACE_ID);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void onPresenterCreated() {
        presenter.setMapView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.setMapView(Presenter.dummyMap);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null && map != null) {
            cameraPosition = map.getCameraPosition();
            outState.putParcelable(CAMERA_POSITION, cameraPosition);
            outState.putInt(UiHelper.PLACE_ID, presenter.getSelectedPlaceId());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.map_help_edit, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem edit = menu.findItem(R.id.menu_edit);
        if (edit != null)
            edit.setVisible(markerManager != null && presenter.getSelectedPlaceId() != Place.NO_PLACE_ID);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
                presenter.showPlaceDetails(presenter.getSelectedPlaceId());
                return true;
            case R.id.menu_help:
                showMapHelp(getActivity());
                return true;
        }
        return false;
    }

    @Override
    public void showPlace(int placeId) {
        placeIdToShow = placeId;
        cameraPosition = null;
        updateMarkers();
    }

    @Override
    public void updateMarkers() {
        if (presenter != null)
            presenter.getPlaces(this);
    }

    @Override
    public void onPlacesLoaded(Place[] places) {
        if (map == null)
            return;
        map.clear();
        markerManager.drawMarkers(places, placeIdToShow);
        if (cameraPosition == null)
            for (Place place: places)
                if (place.id == placeIdToShow) {
                    showPlaceOnMap(map, place);
                    break;
                }
        placeIdToShow = Place.NO_PLACE_ID;
    }

    protected void setupMap(@NotNull GoogleMap map) {
        this.map = map;
        if (cameraPosition != null)
            map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        map.setMyLocationEnabled(true);
        if (markerManager == null)
            markerManager = new MarkerManager(getActivity(), presenter, map, this);
        else
            markerManager.setupMap(map);
        updateMarkers();
    }

    public static String getDescription(Context context, LatLng latLng) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                String locality = address.getLocality();
                if (locality != null)
                    result.append(locality).append(", ");
                result.append(address.getCountryName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    private static void showPlaceOnMap(GoogleMap map, Place place) {
        if (map == null)
            return;

        if (place == null)
            return;

        LatLng latLng = new LatLng(place.latitude, place.longitude);
        CameraUpdate cameraUpdate = map.getCameraPosition().zoom < 10
                ? CameraUpdateFactory.newLatLngZoom(latLng, 10)
                : CameraUpdateFactory.newLatLng(latLng);
        map.animateCamera(cameraUpdate);
    }

    public static void showMapHelp(Context context) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.help)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setMessage(R.string.map_help)
                .setPositiveButton(android.R.string.ok, null)
                .setCancelable(true)
                .show();
    }

    public static Presenter.IMapView createInstance(Context context) {
        if (ConnectionResult.SUCCESS == GooglePlayServicesUtil.isGooglePlayServicesAvailable(context))
            return new MapFragment();
        else
            return new MapUnavailableFragment();
    }
}
