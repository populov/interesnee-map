package com.haunted.interesnee;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.haunted.interesnee.data.ImageFileStorage;
import com.haunted.interesnee.model.ImageUrlCollection;
import com.haunted.interesnee.model.Place;
import com.haunted.interesnee.model.ThumbSize;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MarkerManager implements GoogleMap.OnMarkerDragListener, GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMapLongClickListener {
    private final Context context;
    private final Presenter presenter;
    private final Presenter.IMapView mapView;
    private final GoogleMap map;
    private Map<Marker,Integer> placeIDs = new HashMap<>();
    private Map<Integer,LatLng> positions = new HashMap<>();

    public MarkerManager(@NotNull Context context, @NotNull Presenter presenter, @NotNull GoogleMap map, @NotNull Presenter.IMapView mapView) {
        this.context = context;
        this.presenter = presenter;
        this.map = map;
        this.mapView = mapView;
        setupMap(map);
    }

    public void setupMap(GoogleMap map) {
        map.setOnMapLongClickListener(this);
        map.setOnMarkerDragListener(this);
        map.setOnMarkerClickListener(this);
        map.setOnInfoWindowClickListener(this);
        map.setInfoWindowAdapter(new BalloonAdapter());
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        int placeId = placeIDs.get(marker);
        if (placeId != presenter.getSelectedPlaceId() && presenter.getSelectedPlaceId() != Place.NO_PLACE_ID)
            redrawMarker(presenter.getSelectedPlaceId(), placeId);
        presenter.setPlaceSelected(placeId);
    }

    @Override
    public void onMarkerDrag(Marker marker) {}

    @Override
    public void onMarkerDragEnd(final Marker marker) {
        final int placeId = placeIDs.get(marker);
        if (placeId == Place.NO_PLACE_ID) {
            marker.setTitle(MapFragment.getDescription(context, marker.getPosition()));
            if (marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
                marker.showInfoWindow();
            }
            return;
        }
        new AlertDialog.Builder(context)
                .setTitle(marker.getTitle())
                .setIcon(android.R.drawable.ic_menu_save)
                .setMessage(R.string.save_new_location)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Place.updatePosition(placeId, marker.getPosition(), presenter.placesStorage, presenter.imageStorage);
                        placeIDs.remove(marker);
                        marker.remove();
                        mapView.showPlace(placeId);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Place place = presenter.placesStorage.getPlace(placeId);
                        marker.setPosition(new LatLng(place.latitude, place.longitude));
                    }
                })
                .setCancelable(false)
                .show();
    }

    @Override
    public void onInfoWindowClick(final Marker marker) {
        int placeId = placeIDs.get(marker);
        if (placeId != Place.NO_PLACE_ID) {
            presenter.showPlaceDetails(placeId);
            return;
        }

        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_menu_add)
                .setTitle(R.string.new_place)
                .setMessage(marker.getTitle())
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int placeId = presenter.placesStorage.insertPlace(marker.getTitle(), marker.getPosition().latitude, marker.getPosition().longitude, null, new Date());
                        placeIDs.remove(marker);
                        placeIDs.put(marker, placeId);
                        presenter.onPlacesUpdated();
                        presenter.showPlaceDetails(placeId);
                    }
                })
                .setNegativeButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        marker.remove();
                    }
                })
                .show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        int placeId = placeIDs.get(marker);
        if (presenter.getSelectedPlaceId() != placeId) {
            if (presenter.getSelectedPlaceId() != Place.NO_PLACE_ID)
                redrawMarker(presenter.getSelectedPlaceId(), placeId);
            Marker selected = placeId != Place.NO_PLACE_ID ? redrawMarker(placeId, placeId) : findMarkerByPlaceId(placeId);
            selected.showInfoWindow();
            presenter.setPlaceSelected(placeId);
            return true;
        }
        else
            return false;
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        Marker notSaved = findMarkerByPlaceId(Place.NO_PLACE_ID);
        if (notSaved != null) {
            placeIDs.remove(notSaved);
            notSaved.remove();
        }
        redrawMarker(presenter.getSelectedPlaceId(), Place.NO_PLACE_ID);
        presenter.setPlaceSelected(Place.NO_PLACE_ID);
        Place newPlace = new Place(Place.NO_PLACE_ID, MapFragment.getDescription(context, latLng), latLng.latitude, latLng.longitude, null, new Date());
        Marker marker = addMarker(newPlace, newPlace.id, latLng);
        marker.showInfoWindow();
        presenter.onPlaceSelected();
    }

    private Marker findMarkerByPlaceId(int placeId) {
        for (Map.Entry<Marker,Integer> entry: placeIDs.entrySet())
            if (entry.getValue() == placeId)
                return entry.getKey();
        return null;
    }

    public Marker addMarker(Place place, int selectedId, LatLng position) {
        BitmapDescriptor pin;
        if (place.id == Place.NO_PLACE_ID)
            pin = BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin_add);
        else if (place.id == selectedId)
            pin = BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin_selected);
        else
            pin = BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin_a);
        MarkerOptions markerOptions = new MarkerOptions()
                .position(position)
                .title(place.name)
                .snippet(presenter.dateFormat.format(place.visited))
                .icon(pin)
                .draggable(true);
        Marker marker = map.addMarker(markerOptions);
        placeIDs.put(marker, place.id);
        return marker;
    }

    private Marker redrawMarker(int placeId, int selectedPlaceId) {
        Marker marker = findMarkerByPlaceId(placeId);
        if (marker == null)
            return null;
        placeIDs.remove(marker);
        marker.remove();
        Place place = presenter.placesStorage.getPlace(placeId);
        LatLng markerPosition = positions.containsKey(placeId)
                ? positions.get(placeId)
                : new LatLng(place.latitude, place.longitude);
        return addMarker(place, selectedPlaceId, markerPosition);
    }

    private static final double COLLISION_OFFSET = 0.00005;
    public void drawMarkers(Place[] places, int placeIdToShow) {
        placeIDs.clear();
        positions.clear();
        for (Place place: places) {
            LatLng pos = new LatLng(place.latitude, place.longitude);
            while (positions.containsValue(pos))
                pos = new LatLng(pos.latitude + COLLISION_OFFSET, pos.longitude + COLLISION_OFFSET);
            positions.put(place.id, pos);
            addMarker(place, placeIdToShow, pos);
        }
    }

    private class BalloonAdapter implements GoogleMap.InfoWindowAdapter {
        @Override
        public View getInfoWindow(Marker marker) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View result = inflater.inflate(R.layout.custom_info_window, null);
            assert result != null;
            ImageView image = (ImageView) result.findViewById(R.id.badge);
            TextView tName = (TextView) result.findViewById(R.id.title);
            TextView tVisited = (TextView) result.findViewById(R.id.snippet);

            int placeId = placeIDs.get(marker);
            if (placeId != Place.NO_PLACE_ID)
                renderStoredPlaceWindow(placeId, image, tName, tVisited);
            else
                renderNewPlaceWindow(marker, image, tName, tVisited);

            return result;
        }

        private void renderStoredPlaceWindow(int placeId, ImageView image, TextView tName, TextView tVisited) {
            Place place = presenter.placesStorage.getPlace(placeId);
            tName.setText(place.name);
            tVisited.setText(presenter.dateFormat.format(place.visited));
            String imageUrl = new ImageUrlCollection(place.image).first();
            if (imageUrl != null) {
                try {
                    Bitmap thumb = presenter.imageStorage.getOrCreateThumb(imageUrl, ThumbSize.SMALL, false);
                    if (thumb != null) {
                        image.setImageBitmap(thumb);
                        return;
                    }
                } catch (ImageFileStorage.OriginNotFoundException e) {
                    e.printStackTrace();
                }
            }
            image.setVisibility(View.GONE);
        }

        private void renderNewPlaceWindow(Marker marker, ImageView image, TextView tName, TextView tVisited) {
            image.setImageResource(android.R.drawable.ic_input_add);
            tName.setText(marker.getTitle());
            tVisited.setText(marker.getSnippet());
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    }
}
