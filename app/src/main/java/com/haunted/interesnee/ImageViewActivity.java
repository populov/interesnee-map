package com.haunted.interesnee;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.haunted.interesnee.model.ImageUrlCollection;
import com.haunted.interesnee.model.Place;
import com.haunted.interesnee.model.ThumbSize;

public class ImageViewActivity extends CustomAnimationActivity implements ImageLoadAsyncTask.ImageNotFoundListener {
    public static final String IMAGE_INDEX = "imageIndex";
    private int placeId;
    private Presenter presenter;
    private ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar ab = getSupportActionBar();
        if (ab != null)
            ab.setDisplayHomeAsUpEnabled(true);

        placeId = UiHelper.getPlaceId(getIntent());
        presenter = Presenter.createDefault(this, null, placeId);

        Place place = presenter.getSelectedPlace();
        setTitle(place.name);

        ViewPager viewPager = new ViewPager(this);
        viewPager.setId(R.id.lThumbs);
        imageAdapter = new ImageAdapter(getSupportFragmentManager(), new ImageUrlCollection(place.image), viewPager);
        viewPager.setAdapter(imageAdapter);
        viewPager.setCurrentItem(getIntent().getIntExtra(IMAGE_INDEX, 0));
        setContentView(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.place_delete, menu);
        getMenuInflater().inflate(R.menu.image_primary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateUp();
                return true;
            case R.id.menu_delete:
                new AlertDialog.Builder(this)
                        .setTitle(R.string.delete_image)
                        .setIcon(R.drawable.ic_menu_delete)
                        .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String imageUrl = imageAdapter.getCurrentImageUrl();
                                Place.deleteImage(placeId, imageUrl, presenter.placesStorage, presenter.imageStorage);
                                navigateUp();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
                return true;
            case R.id.menu_set_primary:
                Place.setPrimaryImage(placeId, imageAdapter.getCurrentImageUrl(), presenter.placesStorage, presenter.imageStorage);
                navigateUp();
                return true;
        }
        return false;
    }

    private void navigateUp() {
        onBackPressed();
    }

    private class ImageAdapter extends FragmentPagerAdapter {
        private final String[] urls;
        private final ViewPager viewPager;

        public ImageAdapter(FragmentManager fm, ImageUrlCollection urls, ViewPager viewPager) {
            super(fm);
            this.viewPager = viewPager;
            this.urls = urls.getAll();
        }

        @Override
        public Fragment getItem(int position) {
            return ImageFragment.createInstance(urls[position]);
        }

        @Override
        public int getCount() {
            return urls.length;
        }

        public String getCurrentImageUrl() {
            return urls[viewPager.getCurrentItem()];
        }

        public boolean canSetAsPrimary() {
            return viewPager.getCurrentItem() > 0;
        }
    }

    @Override
    public void onImageNotFound(final String imageUrl) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.image_not_found)
                .setMessage(imageUrl)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Place.deleteImage(presenter.getSelectedPlaceId(), imageUrl, presenter.placesStorage, presenter.imageStorage);
                        navigateUp();
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();

    }

    public static class ImageFragment extends Fragment {
        private static final String IMAGE_URL = "imageUrl";
        private String imageUrl;
        private ImageAdapter imageAdapter;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.activity_image_view, null);
            assert v != null;
            imageUrl = getArguments().getString(IMAGE_URL);
            ImageView image = (ImageView) v.findViewById(R.id.image);
            ImageViewActivity activity = (ImageViewActivity) getActivity();
            imageAdapter = activity.imageAdapter;
            new ImageLoadAsyncTask(image, imageUrl, activity.presenter.imageStorage, ThumbSize.FULL, activity).execute();
            setHasOptionsMenu(true);
            return v;
        }

        @Override
        public void onPrepareOptionsMenu(Menu menu) {
            MenuItem setAsPrimaryImage = menu.findItem(R.id.menu_set_primary);
            if (setAsPrimaryImage != null && imageAdapter != null)
                setAsPrimaryImage.setVisible(imageAdapter.canSetAsPrimary());
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putString(IMAGE_INDEX, imageUrl);
        }

        static ImageFragment createInstance(String imageUrl) {
            Bundle args = new Bundle();
            args.putString(IMAGE_URL, imageUrl);
            ImageFragment result = new ImageFragment();
            result.setArguments(args);
            return result;
        }
    }
}
