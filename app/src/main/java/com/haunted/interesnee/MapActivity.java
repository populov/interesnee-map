package com.haunted.interesnee;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.haunted.interesnee.model.Place;

public class MapActivity extends PresenterActivity implements Presenter.IActivityView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fm = getSupportFragmentManager();
        Presenter.IMapView mapFragment = (Presenter.IMapView) fm.findFragmentById(android.R.id.content);

        if (mapFragment == null) {
            mapFragment = MapFragment.createInstance(this);
            fm.beginTransaction()
                    .replace(android.R.id.content, (Fragment) mapFragment)
                    .commit();
        }
        mapFragment.showPlace(presenter.getSelectedPlaceId());
    }

    @Override
    public void onPlaceSelected() {
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (presenter.getSelectedPlaceId() != Place.NO_PLACE_ID)
                NavUtils.navigateUpTo(this, UiHelper.detailsIntent(this, presenter.getSelectedPlaceId()));
            else
                showList();
            return true;
        }
        return false;
    }

    @Override
    public void showMap(int placeId) {
        throw new UnsupportedOperationException("showMap() should never be called on MapActivity");
    }

    @Override
    public boolean showDetails(int placeId) {
        startActivity(UiHelper.detailsIntent(this, placeId));
        return true;
    }

    @Override
    public void showList() {
        NavUtils.navigateUpTo(this, new Intent(this, PlaceListActivity.class));
    }

    @Override
    public void onPlaceDeleted(int placeId) {
        throw new UnsupportedOperationException("onPlaceDeleted() should never be called on MapActivity");
    }

    @Override
    public boolean isMapMode() {
        return true;
    }

    @Override
    public boolean isTwoPane() {
        return false;
    }
}
