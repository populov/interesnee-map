package com.haunted.interesnee;

import android.os.Bundle;
import android.support.v7.app.ActionBar;

public abstract class PresenterActivity extends CustomAnimationActivity
        implements PresenterFragment.IPresenterActivity {
    protected Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar ab = getSupportActionBar();
        if (ab != null)
            ab.setDisplayHomeAsUpEnabled(true);

        int placeId = UiHelper.getPlaceId(getIntent());
        if (savedInstanceState != null)
            placeId = savedInstanceState.getInt(UiHelper.PLACE_ID, placeId);

        presenter = Presenter.createDefault(this, (Presenter.IActivityView)this, placeId);
    }

    public Presenter getPresenter() {
        return presenter;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(UiHelper.PLACE_ID, presenter.getSelectedPlaceId());
    }
}