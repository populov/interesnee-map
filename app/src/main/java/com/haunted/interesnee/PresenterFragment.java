package com.haunted.interesnee;

import android.support.v4.app.Fragment;

public abstract class PresenterFragment extends Fragment {
    protected Presenter presenter;

    @Override
    public void onResume() {
        super.onResume();
        ensurePresenter();
    }

    interface IPresenterActivity {
        Presenter getPresenter();
    }

    protected void ensurePresenter() {
        if (presenter == null) {
            IPresenterActivity activity = (IPresenterActivity)getActivity();
            presenter = activity.getPresenter();
            if (presenter == null)
                throw new NullPointerException("presenter == null");
            onPresenterCreated();
        }
    }

    protected abstract void onPresenterCreated();
}
