package com.haunted.interesnee;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.haunted.interesnee.model.Place;

public class PlaceListControllerOnePane implements PlaceListActivity.PlaceListController {
    private final FragmentActivity activity;
    private Presenter presenter;
    private PlaceListFragment listFragment;

    public PlaceListControllerOnePane(FragmentActivity activity, PlaceListFragment placeListFragment) {
        this.activity = activity;
        listFragment = placeListFragment;
        placeListFragment.setActivateOnItemClick(false);
    }

    @Override
    public void start(Presenter presenter, Bundle savedState) {
        this.presenter = presenter;
        if (savedState == null)
            listFragment.setItemSelected(presenter.getSelectedPlaceId());
    }

    @Override
    public void showMap(int placeId) {
        activity.startActivity(UiHelper.mapIntent(activity, placeId));
    }

    @Override
    public boolean showDetails(int placeId) {
        activity.startActivity(UiHelper.detailsIntent(activity, placeId));
        return true;
    }

    @Override
    public void showList() {}

    @Override
    public void onPlaceDeleted(int placeId) {}

    @Override
    public boolean isMapMode() {
        return false;
    }

    @Override
    public boolean isTwoPane() {
        return false;
    }

    @Override
    public void onPlaceSelected() {
        activity.supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(MenuInflater menuInflater, Menu menu) {
        menuInflater.inflate(R.menu.switch_to_map, menu);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_map_view) {
            presenter.showPlaceMap(Place.NO_PLACE_ID);
            return true;
        }
        return false;
    }
}
