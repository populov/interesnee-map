package com.haunted.interesnee;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class MapUnavailableFragment extends PresenterFragment implements Presenter.IMapView, View.OnClickListener {
    private int checkResult;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        checkResult = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        View v = inflater.inflate(R.layout.map_unavailable, container, false);
        assert v != null;
        Button button = (Button) v.findViewById(R.id.btnUpdate);
        button.setOnClickListener(this);
        switch (checkResult) {
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                button.setText(R.string.update_google_play_services);
                break;
            default:
                button.setText(R.string.install_google_play_services);
        }
        return v;
    }

    @Override
    public void onClick(View v) {
        GooglePlayServicesUtil.getErrorDialog(checkResult, getActivity(), 0).show();
    }

    @Override
    protected void onPresenterCreated() {
        presenter.setMapView(this);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.map_help_edit, menu);
        menu.removeItem(R.id.menu_help);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
                presenter.showPlaceDetails(presenter.getSelectedPlaceId());
                return true;
        }
        return false;
    }

    @Override
    public void showPlace(int placeId) {}

    @Override
    public void updateMarkers() {}
}
