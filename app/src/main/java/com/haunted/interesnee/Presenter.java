package com.haunted.interesnee;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.haunted.interesnee.data.ImageFileStorage;
import com.haunted.interesnee.data.PlacesDbStorage;
import com.haunted.interesnee.data.PlacesLoader;
import com.haunted.interesnee.model.IPlacesStorage;
import com.haunted.interesnee.model.ImageStorage;
import com.haunted.interesnee.model.Place;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;

public class Presenter {
    public final IPlacesStorage placesStorage;
    public final ImageStorage imageStorage;
    public final DateFormat dateFormat;
    private final FragmentActivity context;
    private final IActivityView activityView;
    private final IPlacesStorage placesStorageForAsyncLoad;
    private IListView listView;
    private IMapView mapView;
    private IDetailsView detailsView;
    private int selectedPlaceId = Place.NO_PLACE_ID;

    private Presenter(@NotNull FragmentActivity context,
                      @NotNull IMapView mapView, @NotNull IDetailsView detailsView,
                      @NotNull IListView listView, IActivityView activityView,
                      int placeId) {
        this.context = context;
        this.activityView = activityView;
        this.listView = listView;
        this.mapView = mapView;
        this.detailsView = detailsView;

        placesStorage = new PlacesDbStorage(context);
        placesStorageForAsyncLoad = new PlacesDbStorage(context);
        imageStorage = new ImageFileStorage(context);
        dateFormat = android.text.format.DateFormat.getDateFormat(context);
        selectedPlaceId = placeId;
    }

    public static Presenter createDefault(@NotNull FragmentActivity context, IActivityView activityView, int placeId) {
        return new Presenter(context, dummyMap, dummyDetails, dummyList, activityView, placeId);
    }

    public void addNewPlace() {
        navigateToPlace(Place.NO_PLACE_ID, true);
    }

    public void deletePlaceIfConfirm(final int placeId) {
        Place place = placesStorage.getPlace(placeId);
        new AlertDialog.Builder(context)
                .setTitle(place.name)
                .setIcon(R.drawable.ic_menu_delete)
                .setMessage(R.string.delete_this_place)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Place.deletePlace(placeId, placesStorage, imageStorage);
                        activityView.onPlaceDeleted(placeId);
                        if (placeId == selectedPlaceId)
                            listView.setItemSelected(selectedPlaceId = Place.NO_PLACE_ID);
                        onPlacesUpdated();
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    public void setPlaceSelected(int placeId) {
        selectedPlaceId = placeId;
        listView.setItemSelected(placeId);
        activityView.onPlaceSelected();
    }

    public void navigateToPlace(int placeId, boolean forceDetails) {
        boolean canSelect = true;
        if (activityView.isMapMode() && !forceDetails)
            mapView.showPlace(placeId);
        else
            canSelect = activityView.showDetails(placeId);
        if (canSelect) {
            selectedPlaceId = placeId;
            listView.setItemSelected(placeId);
            activityView.onPlaceSelected();
        }
        else
            listView.setItemSelected(selectedPlaceId);
    }

    public void savePlaceAndRun(final Runnable action, boolean ignoreEmptyNewPlace) {
        EnteredPlaceData placeData = detailsView.getPlaceData();
        if (placeData == null)
            return;
        if (placeData.anyWarnings()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context)
                    .setTitle(R.string.cant_save)
                    .setMessage(placeData.getMessage(context))
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(R.string.continue_edit, null);
            if (!isNewPlace() || ignoreEmptyNewPlace)
                dialog.setNegativeButton(R.string.dont_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        action.run();
                    }
                });
            dialog.show();
        }
        else if (placeData.nothingChanged()) {
            if (!isNewPlace() || ignoreEmptyNewPlace)
                action.run();
            else
                Toast.makeText(context, R.string.enter_data_first, Toast.LENGTH_SHORT).show();
        }
        else if (placeData.save(placesStorage)) {
            selectedPlaceId = placeData.getPlaceId();
            Toast.makeText(context, R.string.changes_saved, Toast.LENGTH_SHORT).show();
            activityView.onPlaceSelected();
            onPlacesUpdated();
            action.run();
        }
        else Toast.makeText(context, R.string.cant_save, Toast.LENGTH_SHORT).show();
    }

    public void onPlacesUpdated() {
        listView.updatePlacesList();
        mapView.updateMarkers();
    }

    public void onPlaceSelected() {
        activityView.onPlaceSelected();
    }

    public void onPlaceDeleted(int placeId) {
        activityView.onPlaceDeleted(placeId);
    }

    public void setDetailsView(@NotNull IDetailsView detailsView) {
        this.detailsView = detailsView;
    }

    public void setMapView(@NotNull IMapView mapView) {
        this.mapView = mapView;
    }

    public void setListView(@NotNull IListView listView) {
        this.listView = listView;
    }

    public void showPlaceDetails(int placeId) {
        selectedPlaceId = placeId;
        listView.setItemSelected(selectedPlaceId);
        activityView.showDetails(placeId);
    }

    public void showPlaceMap(int placeId) {
        activityView.showMap(placeId);
    }

    public boolean isNewPlace() {
        return selectedPlaceId == Place.NO_PLACE_ID;
    }

    public int getSelectedPlaceId() {
        return selectedPlaceId;
    }

    public Place getSelectedPlace() {
        return isNewPlace() ? null : placesStorage.getPlace(selectedPlaceId);
    }

    public void getPlaces(PlacesLoader.LoadComplete onLoadComplete) {
        PlacesLoader.start(context.getSupportLoaderManager(),
                new PlacesLoader.Callbacks(context, placesStorageForAsyncLoad, onLoadComplete));
    }

    public boolean isTwoPane() {
        return activityView.isTwoPane();
    }

    interface IListView {
        void updatePlacesList();
        void setItemSelected(int placeId);
    }

    interface IMapView {
        void showPlace(int placeId);
        void updateMarkers();
    }

    interface IDetailsView {
        EnteredPlaceData getPlaceData();
        void displayPlace(@NotNull Place place);
    }

    interface IActivityView {
        void showList();
        void showMap(int placeId);
        boolean showDetails(int placeId);
        boolean isMapMode();
        boolean isTwoPane();
        void onPlaceSelected();
        void onPlaceDeleted(int placeId);
    }

    public static final IMapView dummyMap = new IMapView() {
        @Override public void showPlace(int placeId) {}
        @Override public void updateMarkers() {}
    };

    public static final IDetailsView dummyDetails = new IDetailsView() {
        @Override public EnteredPlaceData getPlaceData() { return null; }
        @Override public void displayPlace(@NotNull Place place) {}
    };

    public static final IListView dummyList = new IListView() {
        @Override public void updatePlacesList() {}
        @Override public void setItemSelected(int placeId) {}
    };
}
