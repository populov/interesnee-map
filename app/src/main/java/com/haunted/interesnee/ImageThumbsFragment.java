package com.haunted.interesnee;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.haunted.interesnee.model.ImageStorage;
import com.haunted.interesnee.model.ImageUrlCollection;
import com.haunted.interesnee.model.Place;

import java.util.Collection;

public class ImageThumbsFragment extends PresenterFragment implements ImagesDisplayAsyncTask.ImagesNotFoundListener {
    private static final String IMAGE_URLS = "displayedImageUrls";
    private ImageStorage imageStorage;
    private String displayedImageUrls;
    private LinearLayout thumbsContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            String urls = savedInstanceState != null
                    ? savedInstanceState.getString(IMAGE_URLS)
                    : getArguments().getString(IMAGE_URLS);
        thumbsContainer = (LinearLayout) inflater.inflate(R.layout.fragment_thumbs, null);
        displayThumbs(urls);
        return thumbsContainer;
    }

    @Override
    protected void onPresenterCreated() {
        imageStorage = presenter.imageStorage;
    }

    public void displayThumbs(final String displayImageUrls) {
        final FragmentActivity activity = getActivity();
        if (activity == null)
            return;
        ensurePresenter();
        thumbsContainer.removeAllViews();
        thumbsContainer.post(new Runnable() {
            @Override
            public void run() {
                new ImagesDisplayAsyncTask(activity, imageStorage, thumbsContainer, new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        presenter.savePlaceAndRun(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(UiHelper.imageViewIntent(activity, presenter.getSelectedPlaceId(), (int)v.getTag()));
                            }
                        }, false);

                    }
                }, ImageThumbsFragment.this).execute(new ImageUrlCollection(displayImageUrls).getAll());
                displayedImageUrls = displayImageUrls;
            }
        });
    }

    public String getDisplayedImageUrls() {
        return displayedImageUrls;
    }

    public static ImageThumbsFragment createInstance(String imageUrls) {
        Bundle args = new Bundle();
        args.putString(IMAGE_URLS, imageUrls);
        ImageThumbsFragment result = new ImageThumbsFragment();
        result.setArguments(args);
        return result;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(IMAGE_URLS, displayedImageUrls);
    }

    @Override
    public void onImagesNotFound(Collection<String> notFoundImages) {
        for (final String imageUrl: notFoundImages) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.image_not_found)
                    .setMessage(imageUrl)
                    .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Place.deleteImage(presenter.getSelectedPlaceId(), imageUrl, presenter.placesStorage, imageStorage);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
        }
    }
}
