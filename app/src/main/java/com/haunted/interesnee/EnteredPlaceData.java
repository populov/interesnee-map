package com.haunted.interesnee;

import android.content.Context;
import android.widget.EditText;

import com.haunted.interesnee.model.IPlacesStorage;
import com.haunted.interesnee.model.Place;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EnteredPlaceData {
    private static final double PRECISION = 0.000001;
    private static final double PRECISION_REV = Math.round(1/PRECISION);
    private final Place origin;
    private String name;
    private Double latitude;
    private Double longitude;
    private Date visited;
    private List<Integer> warnings;
    private int placeId;

    @SuppressWarnings("ConstantConditions")
    public EnteredPlaceData(EditText etName, EditText etLatitude, EditText etLongitude, EditText etDate, DateFormat dateFormat, Place origin) {
        this(etName.getText().toString(), etLatitude.getText().toString(),  etLongitude.getText().toString(), etDate.getText().toString(), dateFormat, origin);
    }

    public EnteredPlaceData(String sName, String sLatitude, String sLongitude, String sDate, DateFormat dateFormat, Place origin) {
        this.origin = origin;
        placeId = origin != null ? origin.id : Place.NO_PLACE_ID;
        name = sName;
        try {
            latitude = sLatitude.isEmpty() ? null : Double.parseDouble(sLatitude);
        } catch (NumberFormatException e){
            latitude = null;
            warn(R.string.latitude_incorrect_format);
        }
        try {
            longitude = sLongitude.isEmpty() ? null : Double.parseDouble(sLongitude);
        } catch (NumberFormatException e) {
            longitude = null;
            warn(R.string.longitude_incorrect_format);
        }
        try {
            visited = sDate.isEmpty() ? null : dateFormat.parse(sDate);
        } catch (ParseException e) {
            visited = null;
            warn(R.string.date_incorrect_format);
        }
        if (!anyWarnings())
            verify();
    }

    private void verify() {
        boolean noData = nothingEntered();
        if (origin == null && noData)
            return;
        if (origin != null && noData) {
            warn(R.string.delete_hint);
            return;
        }
        if (name.isEmpty()) {
            warn(R.string.enter_name);
            return;
        }
        if (latitude == null)
            warn(R.string.enter_latitude);
        if (longitude == null)
            warn(R.string.enter_longitude);
        if (visited == null)
            warn(R.string.enter_date);
        if (latitude != null && (latitude < -90 || latitude > 90))
            warn(R.string.latitude_range);
        if (longitude != null && (longitude < -180 || longitude > 180))
            warn(R.string.longitude_range);
    }

    private void warn(int messageId) {
        if (warnings == null)
            warnings = new ArrayList<>();
        warnings.add(messageId);
    }

    private boolean nothingEntered() {
        return name.isEmpty() && latitude == null && longitude == null
                && (visited == null || isToday(visited));
    }

    private boolean isToday(Date date) {
        return sameDate(date, new Date());
    }

    private boolean sameDate(Date d1, Date d2) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(d2);
        return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
                && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)
                && c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH);
    }

    public boolean nothingChanged() {
        if (origin == null)
            return nothingEntered();
        return name.equals(origin.name)
                && Math.abs(latitude - origin.latitude) < PRECISION
                && Math.abs(longitude - origin.longitude) < PRECISION
                && sameDate(visited, origin.visited);
    }

    public boolean anyWarnings() {
        return warnings != null;
    }

    public boolean save(IPlacesStorage placesStorage) {
        if (origin != null)
            return placesStorage.updatePlace(origin.id, name, latitude, longitude, visited);
        else {
            placeId = placesStorage.insertPlace(name, latitude, longitude, null, visited);
            return  0 < placeId;
        }
    }

    public String getMessage(Context context) {
        StringBuilder result = new StringBuilder();
        for(int resourceId: warnings) {
            if (result.length() > 0)
                result.append('\n');
            result.append(context.getString(resourceId));
        }
        return result.toString();
    }

    public int getPlaceId() {
        return placeId;
    }

    public static String coordinateToString(double val) {
        return Double.toString(Math.round(val * PRECISION_REV) / PRECISION_REV);
    }
}
